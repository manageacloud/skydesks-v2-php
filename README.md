#Skydesks V2 PHP Library

This API comprises a partial implementation of [Skydesks PHP API V2](https://docs.google.com/document/d/1q5gmTXPQLI3hjh7R_LTfZcsoRVFZKLiDVW1QuJSgv58/edit?usp=sharing)

The current implementation comprises the following API Calls:

 - Creation of subscriptions
 - List of subscriptions
 - Delete subscriptions
 - Perform broker login
 - Creation of access tokens
 - Configuration of receivers in the context of copy-trading
