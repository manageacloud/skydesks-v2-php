<?php

class Skydesks_Apiv2
{

    // If you don't have an API key, please contact support@skydesks.com
    private $skydesks_api_key = "your skydesks key goes here";
    private $last_error;
    private $debug;

    /**
     * Skydesks_Api constructor.
     *
     * @param $debug int 0 to disable debug functionality, 1 to enable debug.
     */
    public function __construct($debug)
    {
        if (!function_exists("get_the_user_ip")) {
            function get_the_user_ip() {
                if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $ip = $_SERVER['REMOTE_ADDR'];
                }
                return $ip;
            }
        }

        // if the wordpress installation has
        // debug mode enabled, we are running
        // in the development environment
        $this->debug = $debug;


    }

    /**
     * If an end point returns null, it means that something has failed.
     * This method returns the last error.
     *
     * @return mixed
     */
    public function get_last_error()
    {
        return $this->last_error;
    }


    /**
     * Deploys a new subscription using the given product.
     *
     * @param $location string that represents the location where you want to deploy the subscription.
     * More info at https://docs.google.com/document/d/1q5gmTXPQLI3hjh7R_LTfZcsoRVFZKLiDVW1QuJSgv58/edit#heading=h.q054rcmkv0jw
     * @param $product string that represents the product that you want to deploy. By default blank Cloud MT4 and Cloud MT5 are
     * enabled in all the accounts. If you want to have available your pre-configured subscription, you need to contact
     * support@skydesks.com . Once the product is enabled over the API, you can find the name in the property "products"
     * of the API call "Account". More info at https://docs.google.com/document/d/1q5gmTXPQLI3hjh7R_LTfZcsoRVFZKLiDVW1QuJSgv58/edit#heading=h.jyghd9bcwoky
     * @param $callback_url Url to be called by skydesks after the deployment is completed.
     * More info at https://docs.google.com/document/d/1q5gmTXPQLI3hjh7R_LTfZcsoRVFZKLiDVW1QuJSgv58/edit#heading=h.27hqxp7f4izi
     * @return mixed, subscription_id if the request was successfully executed or null if there is an error.
     */
    function deploy_subscription($location, $product, $callback_url)
    {

        $callback_url = $this->deploy_callback_url;
        $url = "https://api.skydesks.com/v2/subscriptions";

        // if debug is set, use the temporary call back URLS.
        // Those URL allows you to see the request from Skydesks
        // if you open https://beeceptor.com/console/test-skydesks
        // before executing the API end point.
        if ($this->debug) {
            $callback_url = "https://test-skydesks.free.beeceptor.com";
        }

        $data = array(
            'product' => $product,
            'location' => $location,
            'callback_url' => $callback_url,
        );

        $result_raw = $this->execute($url, $data);
        $result = json_decode($result_raw);

        if (isset($result->subscription_id)) {
            return $result->subscription_id;
        } else {
            $this->last_error = $result_raw;
            return null;
        }
    }


    /**
     *  Performs the broker login
     *  More info at https://docs.google.com/document/d/1q5gmTXPQLI3hjh7R_LTfZcsoRVFZKLiDVW1QuJSgv58/edit#heading=h.bk8xvaixfcl6
     *
     * @param $subscription_id int subscription id where the login will be performed
     * @param $broker_server string that represents the server of the broker in MT4 format
     * @param $account_number string that represents the broker MT4 account number
     * @param $account_password string that represents the broker MT4 account password
     * @param $callback_url Url to be called by skydesks after the login is completed.
     * @return mixed int subscription id if the API call has been successfully executed, null if there is a problem.
     */
    function broker_login($subscription_id, $broker_server, $account_number, $account_password, $callback_url)
    {

        $url = "https://api.skydesks.com/v2/subscriptions/$subscription_id/actions/login";

        // if debug is set, use the temporary call back URLS.
        // Those URL allows you to see the request from Skydesks
        // if you open https://beeceptor.com/console/test-skydesks
        // before executing the API end point.
        if ($this->debug) {
            $callback_url = "https://test-skydesks.free.beeceptor.com";
        }

        $params = array(
            "callback_url" => $callback_url,
            "server_name" => $broker_server,
            "account_number" => $account_number,
            "account_password" => $account_password,

        );



        $result_raw = $this->execute($url, $params);
        $result = json_decode($result_raw);

        if (isset($result->subscription_id)) {
            return $result->subscription_id;
        } else {
            $this->last_error = $result_raw;
            return null;
        }

    }


    /**
     * Adds a receiver to the copy trader server
     *
     * @param $subscription_id integer receiver's subscription id
     * @param $account_number string account number of the receiver subscription ip
     * @param $provider_code string the code that represents the provider where you want to add the receiver.
     * This code can be found in the property "providers" in the Account API call
     * https://docs.google.com/document/d/1q5gmTXPQLI3hjh7R_LTfZcsoRVFZKLiDVW1QuJSgv58/edit#heading=h.jyghd9bcwoky
     * @return int subscription id if the request has been successfully executed, null if it has failed.
     */
    function add_receiver($subscription_id, $account_number, $provider_code)
    {
        $url = "https://api.skydesks.com/v2/provider/$provider_code/receiver";
        $params = array(
            "receiver_account_number" => $account_number,
            "subscription_id" => $subscription_id
        );

        $result_raw = $this->execute($url, $params);
        $result = json_decode($result_raw);

        if (isset($result->subscription_id)) {
            return $result->subscription_id;
        } else {
            $this->last_error = $result_raw;
            return null;
        }
    }

    /**
     * Gets an access token to view the subscription. Once you have created token, you can see the subscription
     * by opening the following URL in the browser:
     * https://go.skydesks.com/?token=<token>
\    *
     * More info about tokens
     * https://docs.google.com/document/d/1q5gmTXPQLI3hjh7R_LTfZcsoRVFZKLiDVW1QuJSgv58/edit#heading=h.2e276pkvnexp
     *
     * @param $subscription_id
     * @return string token if the request has been successfully executed, or null if the request had a problem.
     */
    function get_token($subscription_id)
    {

        $url = "https://api.skydesks.com/v2/tokens";

        if ( $this->debug ) {
            $remote_ip = '121.45.75.248'; // any random public IP will do for testing purposes
        } else {
            $remote_ip = get_the_user_ip();
        }

        $params = array(
            "subscription_id" => $subscription_id,
            "ip" => $remote_ip
        );

        $result_raw = $this->execute($url, $params);
        $result = json_decode($result_raw);

        if (isset($result->token)) {
            return $result->token;
        } else {
            $this->last_error = $result_raw;
            return null;
        }
    }

    /**
     * Delete a subscription
     *
     * @param $subscription_id
     * @return boolean after the request has been successfully executed
     */
    function delete_subscription($subscription_id)
    {

        $url = "https://api.skydesks.com/v2/subscriptions/$subscription_id";
        $params = array();

        $this->execute($url, $params, "DELETE", false);

        return true;
    }


    private function execute($url, $request_params, $method = "POST", $ignore_errors = true)
    {
        $skydesks_api_key = $this->skydesks_api_key;
        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n" .
                    "Authorization: Bearer $skydesks_api_key\r\n",
                'method' => $method,
                'content' => http_build_query($request_params),
                'ignore_errors' => $ignore_errors
            )
        );
        $context = stream_context_create($options);
        $result_raw = file_get_contents($url, false, $context);

        return $result_raw;
    }

}
